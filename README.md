# Python WebScraping

ws: conjunto de herramientas y técnicas que permiten procesar grandes cantidades de información a través de la web. Son excelentes pasarelas que extienden el uso tradicional de las peticiones html, la templarización css o la interactividad js y eleva el rango de posibilidades de nuestros bots o spiders. Es conveniente antes de diseñar un WebScrawler verificar si el SITE no proporciona una API que provea lo deseado. ¿Qué hace un WebScraper?:

* Recive data HTML desde un dominio especificado.
* Clasifica dicha información.
* Almacena dicha información.
* Opcionalmente se mueve a otros SITES y repite la operación.

### Enlaces de interés:

* [urllib](https://docs.python.org/3/library/urllib.html)
* [beautifulsoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

@jorgemustaine 2018
