from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup as bs


def getTitle(url):
    try:
        html = urlopen(url)
    except:
    # except HTTPError as e:
        return None
    try:
        bsObj = bs(html.read(), features="html.parser")
        title = bsObj.body.h1
    except AttributeError as e:
        return None
    return title

title = getTitle("http://www.pythonscraping.com/pages/page1.html")

if title == None:
    print('Title could not be found! ')
else:
    print(title)
